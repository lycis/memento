import Vue from 'vue'
import App from './App.vue'
import VueCookies from 'vue-cookies'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

Vue.use(VueCookies)
Vue.$cookies.config('60d')

Vue.config.productionTip = false

const routes = [
  // { path: '/login', component: LoginForm },
]
const router = new VueRouter({
  routes // short for `routes: routes`
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
